package com.project.bucketmanager.E2E;

import com.project.bucketmanager.ExceptionHandler.Exceptions.InvalidBearerTokenException;
import com.project.bucketmanager.Models.BucketDetails;
import com.project.bucketmanager.Models.LoginRequest;
import com.project.bucketmanager.Models.LoginResponse;
import com.project.bucketmanager.TestContainersConfig.LocalStackContainerConfig;
import com.project.bucketmanager.TestContainersConfig.PostgreSQLContainerConfig;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.springframework.web.client.RestTemplate;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.containers.localstack.LocalStackContainer;
import org.testcontainers.junit.jupiter.Container;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
class BucketControllerEndToEndTest {
    private static final String bucketname = "bucket-test1";
    private static final LocalStackContainerConfig localStackContainerConfig = new LocalStackContainerConfig(LocalStackContainer.Service.S3);
    private static final PostgreSQLContainerConfig postgreSqlContainerConfig = new PostgreSQLContainerConfig(
            "bucketmanager","postgres","postgres");
    private RestTemplate restTemplate;
    public String jwtToken;
    @Container
    private static final LocalStackContainer localStackContainer = localStackContainerConfig.getLocalStackContainer();
    @Container
    private static final PostgreSQLContainer postgreSqlContainer = postgreSqlContainerConfig.getPostgreSQLContainer();

    @LocalServerPort
    private int port;
    @DynamicPropertySource
    static void connectLocalStack(DynamicPropertyRegistry dynamicPropertyRegistry){
        postgreSqlContainerConfig.setPostgreSQLContainerConnection(dynamicPropertyRegistry);
        localStackContainerConfig.setLocalStackConnectionsConfig(dynamicPropertyRegistry);
    }
    @BeforeEach
    void setUp(){
        restTemplate = new RestTemplate();
        LoginRequest loginRequest = new LoginRequest("read","read123");
        String url = "http://localhost:"+port;
        LoginResponse response = restTemplate.postForEntity(url+"/auth", loginRequest, LoginResponse.class).getBody();
        if (response == null) {
            throw new InvalidBearerTokenException("Error Bearer Token");
        }
        jwtToken = response.bearerToken();
    }
    @Test
    void containersAreUp(){
        assertThat(postgreSqlContainer.isRunning()).isTrue();
        assertThat(localStackContainer.isRunning()).isTrue();
    }

    @Test
    void getAuthToken() {
        assertThat(jwtToken.length()).isNotZero();
    }

    @Test
    void listAllBuckets(){
        String url = "http://localhost:"+port;
        BucketDetails[] buckets = restTemplate.getForObject(url+"/bucket/list",BucketDetails[].class);
        assertThat(buckets.length).isNotZero();
    }
}
