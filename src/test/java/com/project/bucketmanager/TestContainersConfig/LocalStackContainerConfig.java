package com.project.bucketmanager.TestContainersConfig;

import org.springframework.test.context.DynamicPropertyRegistry;
import org.testcontainers.containers.localstack.LocalStackContainer;
import org.testcontainers.utility.DockerImageName;


public class LocalStackContainerConfig {
    private LocalStackContainer localStackContainer;
    private final LocalStackContainer.Service[] services;
    public LocalStackContainerConfig(LocalStackContainer.Service... services) {
        this.services = services;
    }
    public LocalStackContainer getLocalStackContainer(){
        localStackContainer = new LocalStackContainer(
                DockerImageName.parse("localstack/localstack:latest")
        ).withServices(services);
        localStackContainer.start();
        return localStackContainer;
    }
    public void setLocalStackConnectionsConfig(DynamicPropertyRegistry dynamicPropertyRegistry){
        dynamicPropertyRegistry.add("aws.accessKeyId", localStackContainer::getAccessKey);
        dynamicPropertyRegistry.add("aws.secretAccessKey", localStackContainer::getSecretKey);
        dynamicPropertyRegistry.add("aws.bucket.endpoint", localStackContainer::getEndpoint);
        dynamicPropertyRegistry.add("aws.sns.endpoint", localStackContainer::getEndpoint);
    }
}
