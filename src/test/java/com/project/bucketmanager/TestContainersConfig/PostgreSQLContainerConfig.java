package com.project.bucketmanager.TestContainersConfig;

import org.springframework.test.context.DynamicPropertyRegistry;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.utility.DockerImageName;

public class PostgreSQLContainerConfig {
    private final String dbname;
    private final String dbpassword;
    private final String dbusername;
    private PostgreSQLContainer container;
    public PostgreSQLContainerConfig(String dbname, String dbpassword, String dbusername) {
        this.dbname = dbname;
        this.dbpassword = dbpassword;
        this.dbusername = dbusername;
    }

    public PostgreSQLContainer getPostgreSQLContainer(){
        container = new PostgreSQLContainer<>(
                DockerImageName.parse("postgres:13-alpine")
        )
                .withDatabaseName(this.dbname)
                .withUsername(this.dbusername)
                .withPassword(this.dbpassword);
        container.start();
        return container;
    }
    public void setPostgreSQLContainerConnection(DynamicPropertyRegistry dynamicPropertyRegistry){
        dynamicPropertyRegistry.add("spring.datasource.url",container::getJdbcUrl);
        dynamicPropertyRegistry.add("spring.datasource.password",container::getPassword);
        dynamicPropertyRegistry.add("spring.datasource.username",container::getUsername);
    }
}
