package com.project.bucketmanager.Integration;

import com.project.bucketmanager.Models.Log;
import com.project.bucketmanager.Repository.LogsRepository;
import com.project.bucketmanager.TestContainersConfig.LocalStackContainerConfig;
import com.project.bucketmanager.TestContainersConfig.PostgreSQLContainerConfig;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.containers.localstack.LocalStackContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

import java.util.List;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;
@SpringBootTest
@Testcontainers
@ActiveProfiles("test")
public class LogRepositoryIT {

    @Autowired
    private LogsRepository logsRepository;

    private static final PostgreSQLContainerConfig postgreSQLContainerConfig = new PostgreSQLContainerConfig(
            "bucketmanager","postgres","postgres"
    );

    private final List<Log> mockLogs = List.of(
            new Log("bucket1","user1","operation1","time1","ex1"),
            new Log("bucket2","user2","operation2","time2","ex2"));
    private static final LocalStackContainerConfig localStackContainerConfig = new LocalStackContainerConfig(LocalStackContainer.Service.S3);
    @Container
    private static final LocalStackContainer localStackContainer = localStackContainerConfig.getLocalStackContainer();
    @Container
    private static final PostgreSQLContainer postgreSQLContainer = postgreSQLContainerConfig.getPostgreSQLContainer();

    @DynamicPropertySource
    static void properties(DynamicPropertyRegistry dynamicPropertyRegistry){
        localStackContainerConfig.setLocalStackConnectionsConfig(dynamicPropertyRegistry);
        postgreSQLContainerConfig.setPostgreSQLContainerConnection(dynamicPropertyRegistry);
    }

    @Test
    void containerUp(){
        boolean running = postgreSQLContainer.isRunning();
        assertThat(running).isTrue();
    }

    @Test
    @Rollback(value = false)
    void createAndList(){
        mockLogs.forEach(logsRepository::create);
        List<Log> logs = logsRepository.listAllLogs();
        assertThat(logs.size()).isEqualTo(mockLogs.size());
    }

}
