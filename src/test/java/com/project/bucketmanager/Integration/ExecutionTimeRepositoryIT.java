package com.project.bucketmanager.Integration;

import com.project.bucketmanager.Models.ExecutionTime;
import com.project.bucketmanager.Repository.ExecutionTimeRepository;
import com.project.bucketmanager.TestContainersConfig.LocalStackContainerConfig;
import com.project.bucketmanager.TestContainersConfig.PostgreSQLContainerConfig;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.containers.localstack.LocalStackContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;


import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@SpringBootTest
@Testcontainers
@ActiveProfiles("test")
public class ExecutionTimeRepositoryIT {

    @Autowired
    private ExecutionTimeRepository executionTimeRepository;

    private final static List<ExecutionTime> mockExecutionTimes = List.of(
            new ExecutionTime("method1",1L,1L,1L),
            new ExecutionTime("method2",2L,2L,2L)
    );

    private static final PostgreSQLContainerConfig postgreSQLContainerConfig = new PostgreSQLContainerConfig(
            "bucketmanager","postgres","postgres");
    private static final LocalStackContainerConfig localStackContainerConfig = new LocalStackContainerConfig(LocalStackContainer.Service.S3);
    @Container
    private static final LocalStackContainer localStackContainer = localStackContainerConfig.getLocalStackContainer();
    @Container
    private static PostgreSQLContainer postgreSQLContainer =  postgreSQLContainerConfig.getPostgreSQLContainer();

    @DynamicPropertySource
    static void properties(DynamicPropertyRegistry dynamicPropertyRegistry){
        localStackContainerConfig.setLocalStackConnectionsConfig(dynamicPropertyRegistry);
        postgreSQLContainerConfig.setPostgreSQLContainerConnection(dynamicPropertyRegistry);
    }

    @BeforeEach
    void setUp(){
        createRecords();
    }
    @AfterEach
    void tearDown(){
        removeRecords();
    }

    @Test
    void containerUp(){
        boolean running = postgreSQLContainer.isRunning();
        assertThat(running).isTrue();
    }

    @Test
    void listAll(){
        List<ExecutionTime> executionTimes = executionTimeRepository.listAll();
        assertThat(executionTimes.size()).isEqualTo(mockExecutionTimes.size());
    }

    @Test
    void getTheLongestRecord(){
        ExecutionTime executionTime = executionTimeRepository.getTheLongestRecord();
        assertThat(executionTime.getMethodName()).isEqualTo("method2");
    }

    private void createRecords(){
        mockExecutionTimes.forEach(executionTimeRepository::create);
    }
    private void removeRecords(){
        List<Long> ids = executionTimeRepository.listAll()
                .stream()
                .map(ExecutionTime::getId)
                .toList();
        executionTimeRepository.deleteByIds(ids);
    }
}
