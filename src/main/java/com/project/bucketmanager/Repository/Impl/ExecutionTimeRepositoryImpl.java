package com.project.bucketmanager.Repository.Impl;

import com.project.bucketmanager.Models.AverageExecutionTime;
import com.project.bucketmanager.Models.ExecutionTime;
import com.project.bucketmanager.Repository.ExecutionTimeRepository;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public class ExecutionTimeRepositoryImpl implements ExecutionTimeRepository {
    private final JdbcTemplate jdbcTemplate;
    public ExecutionTimeRepositoryImpl(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }
    @Override
    @Transactional
    public void create(ExecutionTime executionTime) {
        String sql = """
            INSERT INTO execution_time (method_name, time) VALUES (?, ?)
        """.trim();
        jdbcTemplate.update(
                sql,
                executionTime.getMethodName(),
                executionTime.getExecutionTime()
        );
    }

    @Override
    @Transactional(readOnly = true)
    public List<ExecutionTime> listAll() {
        String sql = """
            SELECT * FROM execution_time ORDER BY id      
        """;
        return jdbcTemplate.query(sql, (rs, l) -> {
            ExecutionTime executionTime = new ExecutionTime();
            executionTime.setId(Long.valueOf(rs.getString("id")));
            executionTime.setExecutionTime(Long.parseLong(rs.getString("time")));
            executionTime.setMethodName(rs.getString("method_name"));
            return executionTime;
        });
    }

    @Override
    public ExecutionTime getTheLongestRecord() {
        String sql = """
                SELECT * FROM execution_time ORDER BY time DESC limit 1;
        """.trim();
        List<ExecutionTime> executionTimes = jdbcTemplate.query(sql, (rs, rowNum) -> {
            ExecutionTime executionTime = new ExecutionTime();
            executionTime.setId(Long.valueOf(rs.getString("id")));
            executionTime.setExecutionTime(Long.parseLong(rs.getString("time")));
            executionTime.setMethodName(rs.getString("method_name"));
            return executionTime;
        });

        if (!executionTimes.isEmpty()) {
            return executionTimes.get(0);
        } else {
            return null;
        }
    }

    @Override
    public AverageExecutionTime getTheAverageExecutionTime() {
        String sql = """
                    select avg(execution_time) as averageExecutionTime
                            from execution_time
                            limit 20;
        """;
        return jdbcTemplate.query(sql,(rs)->{
            long time = Long.parseLong(rs.getString("averageExecutionTime"));
            return new AverageExecutionTime(time);
        });
    }

    @Override
    public void deleteById(Long id) {
        String sql = """
            delete from execution_time where id = (?)      
        """.trim();
        jdbcTemplate.update(sql, id);
    }
    public void deleteByIds(List<Long> ids){
        String sql = """
            DELETE FROM execution_time WHERE id IN (:ids)
        """;
        NamedParameterJdbcTemplate namedTemplate = new NamedParameterJdbcTemplate(jdbcTemplate);
        MapSqlParameterSource parameters = new MapSqlParameterSource("ids", ids);
        namedTemplate.update(sql, parameters);
    }
}
